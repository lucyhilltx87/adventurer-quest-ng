import { Component, OnInit, Output, EventEmitter} from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-generic-modal',
  templateUrl: './generic-modal.component.html',
  styleUrls: ['./generic-modal.component.scss']
})
export class GenericModalComponent implements OnInit {

  title = '';
  body = '';
  showOKButton = false;
  showCancelButton = false;

  @Output()
  okButtonClicked = new EventEmitter();

  constructor(
    public activeModal: NgbActiveModal
  ) { }

  ngOnInit() {
  }

  close() {
    this.activeModal.dismiss();
  }

  confirm() {
    this.okButtonClicked.emit();
    this.activeModal.dismiss();
  }

}
