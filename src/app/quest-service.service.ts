import { Injectable } from '@angular/core';
import { isFormattedError } from '@angular/compiler';
export class QuestResults {
  statRaised = '';
  displayStatRaised = '';
  statRaisedBy = 0;
  items = [];
}

@Injectable({
  providedIn: 'root'
})
export class QuestService {
  statRaised = "";
  statRaisedBy = 0;

  private questResults;
  constructor() { }
  
  processQuestCompletion(quest, dataService) {
    this.questResults = new QuestResults();
    this.questResults.statRaised = quest.statisticImproved;
    this.questResults.statRaisedBy = quest.statisticImprovedBy;
    this.questResults.displayStatRaised = quest.statisticImproved.substr(0,1) + quest.statisticImproved.substr(1);
    quest.hasFinished = true;
    this.questResults.items = this.processItems(quest, dataService);
    return this.questResults;
  }

  processItems(quest, dataService) {
    const items = [];
    const bucket = dataService.getItemKeysFromBucket(quest.itemBucket);
    let itemCounter = 0;
    for(const key of bucket) {
      const item = dataService.getItemFromKey(key);
      const random = Math.floor(Math.random() * 10);
      if(random <= item.chance) {
        items.push(item);
        itemCounter++;
        const randomForAdditionalChance = Math.floor(Math.random() * 10);
        if(randomForAdditionalChance > 5 || itemCounter >= quest.numberOfItems) {
          break;
        }
      }
    }
    return items;
  }
}


