import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestChooserComponent } from './quest-chooser.component';

describe('QuestChooserComponent', () => {
  let component: QuestChooserComponent;
  let fixture: ComponentFixture<QuestChooserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestChooserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestChooserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
