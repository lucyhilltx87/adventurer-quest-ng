import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { PlayerService } from '../player.service';
import { TimeService } from '../time.service';

@Component({
  selector: 'app-quest-chooser',
  templateUrl: './quest-chooser.component.html',
  styleUrls: ['./quest-chooser.component.scss']
})
export class QuestChooserComponent implements OnInit {

  @Output()
  questStartedEvent = new EventEmitter();

  constructor(
    private playerService: PlayerService,
    private timeService: TimeService,
    public activeModal: NgbActiveModal
  ) { }

  ngOnInit() {
  }

  close() {
    this.activeModal.dismiss();
  }

  startQuest(quest) {
    this.timeService.currentQuest = quest;
    this.timeService.eventName = 'Quest';
    this.timeService.eventFinish = new Date().setHours(new Date().getHours() + 18);
    this.timeService.eventProcessed = false;
    console.log(this.timeService);
    this.timeService.save();
    this.activeModal.dismiss();
    this.questStartedEvent.emit();
  }

  getQuestDifficulty(quest) {
    return Math.floor((quest.level / 2) * (quest.numberOfEnemies * 5 + quest.numberOfEvents * 3));
}

}
