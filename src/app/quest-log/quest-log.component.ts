import { Component, OnInit } from '@angular/core';
import { PlayerService } from '../player.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-quest-log',
  templateUrl: './quest-log.component.html',
  styleUrls: ['./quest-log.component.scss']
})
export class QuestLogComponent implements OnInit {

  constructor(
    private playerService: PlayerService,
    public activeModal: NgbActiveModal
  ) { }

  ngOnInit() {
  }

  close() {
    this.activeModal.dismiss();
  }

}
