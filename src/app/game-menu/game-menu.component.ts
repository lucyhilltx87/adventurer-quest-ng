import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataService } from '../data.service';
import { TimeService } from '../time.service';
import { PlayerService } from '../player.service';
import { isFormattedError, InvokeFunctionExpr } from '@angular/compiler';
import { QuestChooserComponent } from '../quest-chooser/quest-chooser.component';
import { QuestLogComponent } from '../quest-log/quest-log.component';
import { QuestService, QuestResults } from '../quest-service.service';
import { GenericModalComponent } from '../generic-modal/generic-modal.component';

@Component({
  selector: 'app-game-menu',
  templateUrl: './game-menu.component.html',
  styleUrls: ['./game-menu.component.scss']
})
export class GameMenuComponent implements OnInit {
  isEventRunning = false;
  isEventFinish  = false;
  questResult: QuestResults = new QuestResults();
  eventResult = "";

  constructor(
    public timeService: TimeService,
    public playerService: PlayerService,
    public dataService: DataService,
    public modalService: NgbModal,
    public questService: QuestService,
  ) { }

  ngOnInit() {
    const now = new Date().getTime();
    this.timeService.load();
    if (now < this.timeService.eventFinish) {
      console.log("now is less than eventFinish, so event still going");
      this.isEventRunning = true;
    } else {
      console.log("eventFinish is more than now, so event has finished");
      this.isEventFinish = true;
      if (!this.timeService.eventProcessed) {
        console.log("processing event");
        this.processEventCompletion();
      }
    }
    this.playerService.load();
  }

  processEventCompletion() {
      if (this.timeService.eventName === 'Eat') {
        this.eventResult = 'You feel full!  Your hunger is now 0!';
        this.playerService.hunger = 100;
        this.playerService.rest = this.playerService.rest - 10;
        this.playerService.exp += 1;
      }

      if(this.timeService.eventName === 'Talk') {
        this.eventResult = 'You talked to a lot of different people in the tavern.';
        const newQuest = this.getPlayerLearnedQuest();
        if(newQuest != null && this.playerService.availableQuests.indexOf(newQuest) === -1) {
          this.eventResult =
          `${this.eventResult} You talked to someone who knows of a new quest for you!  You can go on the '${newQuest.name} Quest'!`;
          this.playerService.availableQuests.push(newQuest);
        }
        this.playerService.hunger = this.playerService.hunger - 30;
        this.playerService.rest = this.playerService.rest - 10;
        this.playerService.exp += 2;
      }

      if(this.timeService.eventName === 'Sleep') {
        this.eventResult = 'You had a nice sleep.  Your rest is now 100!';
        this.playerService.hunger = this.playerService.hunger - 60;
        this.playerService.rest = 100;
        this.playerService.exp += 1;
      }
      
      if(this.timeService.eventName === 'Quest') {
        const quest = this.timeService.currentQuest;
        const res = this.questService.processQuestCompletion(quest, this.dataService);
        this.playerService[res.statRaised] += res.statRaisedBy;
        this.playerService.exp += quest.exp;
        this.eventResult = `Your ${res.displayStatRaised} increased by ${res.statRaisedBy}!`;
        this.eventResult = `${this.eventResult} Your eLvl was raised by ${quest.exp}!`;
        this.playerService.finishedQuests.push(quest);
        for(const item of res.items) {
          this.eventResult = `${this.eventResult} \n You found a ${item.name}!`;
          this.playerService.items.push(item);
        }
        this.removeAvailableQuest(quest);
            }

      if(this.playerService.hunger <= 0) {
        this.eventResult = `${this.eventResult} You are starving!  You better eat!`;
        this.playerService.hunger = 99;
      }

      if(this.playerService.rest <= 0) {
        this.eventResult = `${this.eventResult} You are exhausted!  You better sleep!`;
        this.playerService.rest = 1;
      }
      this.timeService.eventProcessed = true;
      this.timeService.save();
      this.playerService.save();
  }

  removeAvailableQuest(quest) {
    const idx = this.playerService.availableQuests.indexOf(quest);
    this.playerService.availableQuests.splice(idx);
  }

  getPlayerLearnedQuest() {
    const calcLevel = this.playerService.exp;
    const newQuests =  this.dataService.getQuestsFromLevel(calcLevel);
    let newQuest = null;
    if (newQuests.length > 1) {
      for (const nq of newQuests) {
        const newQuestKey = nq.key;
        if(!this.playerService.getPlayerUnlockedQuest(newQuestKey)) {
            newQuest = this.dataService.getQuestFromKey(newQuestKey);
            break;
        }
      }
    } else if (newQuests.length === 1){
      if(!this.playerService.getPlayerUnlockedQuest(newQuests[0].key)) {
        newQuest = this.dataService.getQuestFromKey(newQuests[0].key);
      }
    }

    return newQuest;
  } 

  processEvent(eventName, numOfHours) {
    this.isEventRunning = true;
    this.isEventFinish = false;
    this.timeService.eventName = eventName;
    this.timeService.eventFinish = new Date().setHours(new Date().getHours() + numOfHours);
    this.timeService.eventProcessed = false;
    this.timeService.save();
  }

  getEventName(): string {
    if (this.timeService.eventName === 'Eat') {
      return 'Eating a Meal';
    }
    if (this.timeService.eventName === 'Sleep') {
      return 'Sleeping';
    }

    if (this.timeService.eventName === 'Talk') {
      return 'Socializing';
    }

    if (this.timeService.eventName === 'Quest') {
      return `Questing [${this.timeService.currentQuest.name}]`;
    }
  }

  processItem(item) {
    if(item.action === 'sell') {
        this.displayGenericModal(`Sell ${item.name}?`, 
        `You are going to sell ${item.name} for ${item.value} silver.  Proceed?`,
        true,
        true,
        () => {
          this.playerService.silver += item.value;
          const indexOfItem = this.playerService.items.indexOf(item);
          this.playerService.items.splice(indexOfItem);
          this.playerService.save();
        });
        return;
    }
  }

  displayQuestChooser() {
    const modal = this.modalService.open(QuestChooserComponent);
    modal.componentInstance.questStartedEvent.subscribe(() => {
        this.isEventRunning = true;
    });
  }

  displayQuestLog() {
    this.modalService.open(QuestLogComponent);
  }

  displayGenericModal(title, body, showOKButton: boolean, showCancelButton: boolean, onConfirm) {
    const modal = this.modalService.open(GenericModalComponent);
    modal.componentInstance.title = title;
    modal.componentInstance.body = body;
    modal.componentInstance.showOKButton = showOKButton;
    modal.componentInstance.showCancelButton = showCancelButton;

    modal.componentInstance.okButtonClicked.subscribe(() => {
      onConfirm();
    });
  }

}
