import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinishTaskComponentComponent } from './finish-task-component.component';

describe('FinishTaskComponentComponent', () => {
  let component: FinishTaskComponentComponent;
  let fixture: ComponentFixture<FinishTaskComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinishTaskComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinishTaskComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
