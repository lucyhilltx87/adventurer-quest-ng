import { Component, OnInit } from '@angular/core';
import { TimeService } from '../time.service';
import { PlayerService } from '../player.service';

@Component({
  selector: 'app-finish-task-component',
  templateUrl: './finish-task-component.component.html',
  styleUrls: ['./finish-task-component.component.scss']
})
export class FinishTaskComponentComponent implements OnInit {

  constructor(
    private timeService: TimeService,
    private playerService: PlayerService,
  ) { }

  ngOnInit() {
    this.playerService.load();
    this.timeService.load();
    this.timeService.eventFinish = new Date().getTime();
    this.timeService.save();
    this.playerService.save();
  }

}
