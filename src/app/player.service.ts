import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {
  name = 'Adventurer';
  hp = 100;
  maxHP = 100;
  attack = 5;
  defense = 6;
  rest = 100;
  hunger = 100;
  exp = 0;
  silver = 0;
  items = [];
  availableQuests = [];
  finishedQuests = [];

  constructor() { 
    if(localStorage.name) {
      this.load();
    } else {
      this.save();
    }
  }

  getPlayerHasQuest(questKey) {
    return this.availableQuests.find(q => q.key === questKey);
  }

  getPlayerHasFinishedQuest(questKey) {
    return this.finishedQuests.find(q => q.key === questKey);
  }

  getPlayerUnlockedQuest(questKey) {
    return this.getPlayerHasQuest(questKey) || this.getPlayerHasFinishedQuest(questKey);
  }

  save() {
    localStorage.name = this.name;
    localStorage.hp = this.hp;
    localStorage.maxHP = this.maxHP;
    localStorage.attack = this.attack;
    localStorage.defense = this.defense;
    localStorage.rest = this.rest;
    localStorage.hunger = this.hunger;
    localStorage.exp = this.exp;
    localStorage.silver = this.silver;
    localStorage.availableQuests = JSON.stringify(this.availableQuests);
    localStorage.finishedQuests = JSON.stringify(this.finishedQuests);
    localStorage.items = JSON.stringify(this.items);
  }

  load() {
    if (!localStorage.name) {
      return;
    }
    this.name = localStorage.name;
    this.hp = localStorage.hp * 1;
    this.maxHP = localStorage.maxHP * 1;
    this.attack = localStorage.attack * 1;
    this.defense = localStorage.defense * 1;
    this.rest = localStorage.rest * 1;
    this.hunger = localStorage.hunger * 1;
    this.exp = localStorage.exp * 1;
    this.silver = localStorage.silver * 1;
    if(localStorage.availableQuests) {
      this.availableQuests = JSON.parse(localStorage.availableQuests);
    }
    if(localStorage.finishedQuests) {
      this.finishedQuests = JSON.parse(localStorage.finishedQuests);
    }
    if(localStorage.items) {
      this.items = JSON.parse(localStorage.items);
    }
  }
}
