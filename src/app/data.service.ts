import { Injectable } from '@angular/core';
import * as dataQuests from './data/quests.json';
import * as dataItemBuckets from './data/itemBuckets.json';
import * as dataItems from './data/items.json';
import { Quest } from './objects/quest.js';
import { Bucket } from './objects/bucket.js';
import { Item } from './objects/item.js';
@Injectable({
  providedIn: 'root'
})
export class DataService {
  quests = [];
  levelQuestMap = [];
  itemBuckets = [];
  items = [];

  constructor() { 
    this.load();
  }

  load() {
    dataQuests.Quests.map((q) => this.quests.push(new Quest(q)));
    dataItemBuckets.ItemBuckets.map((ib) => this.itemBuckets.push(new Bucket(ib)));
    dataItems.Items.map((i) => this.items.push(new Item(i)));
  }

  getQuestFromKey(questKey) {
    return this.quests.find( quest => quest.key === questKey);
  }

  getItemFromKey(key) {
    return this.items.find( item => item.key === key);
  }

  getQuestsFromLevel(pLevel) {
    return this.quests.filter((quest) => quest.level <= pLevel);
  }

  getItemKeysFromBucket(bucketName) {
    const bucket = this.itemBuckets.find( b => b.name === bucketName);
    return bucket.contents;
  }
}
