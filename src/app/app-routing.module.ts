import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GameMenuComponent } from './game-menu/game-menu.component';
import { AppComponent } from './app.component';
import { FinishTaskComponentComponent } from './finish-task-component/finish-task-component.component';
import { RefreshCharacterComponent } from './refresh-character/refresh-character.component';

const routes: Routes = [
  {
    path: 'reset', component: RefreshCharacterComponent
  },
  {
    path: 'game', component: GameMenuComponent
  },
  {
    path: 'finish', component: FinishTaskComponentComponent
  },
  {
    path: '', component: AppComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})


export class AppRoutingModule { }
