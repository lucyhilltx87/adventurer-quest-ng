import { Injectable } from '@angular/core';
import { PlayerService } from './player.service';
import { isNumber } from '@ng-bootstrap/ng-bootstrap/util/util';
import { isFormattedError } from '@angular/compiler';
@Injectable({
  providedIn: 'root'
})
export class UsecodeRunnerService {
  onQuestModifier = {
    enabled: false,
    use: []
  };
  event = {
    enabled: false,
    trigger: '',
    use: []
  };
  constructor() { }

  run(useCodeArray, playerService: PlayerService) {
    for(const useCodeStatement of useCodeArray) {
      const splitStatement = useCodeStatement.split(' ');
      if(this.onQuestModifier.enabled) {
        this.onQuestModifier.use.push(useCodeStatement);
        continue;
      }
      if(this.event.enabled) {
        this.event.use.push(useCodeStatement);
        continue;
      }
      if(splitStatement[0] === 'INCREASE') {
        this.increase(splitStatement[1], splitStatement[2], playerService);
      }
      if(splitStatement[0] === 'INCREASEHP') {
        this.increaseHP(splitStatement[1], playerService);
      }
      if(splitStatement[0] === 'ON' && splitStatement[1] === 'QUEST') {
        this.onQuestModifier.enabled = true;
      }
      if(splitStatement[0] === 'IF') {
        this.event.enabled = true;
        this.event.trigger = splitStatement[1];
      }
      
    }
  }

  private increase(operand, value, playerService) {
    const v = this.verifyValue(value, playerService);
    playerService[operand] += v;
  }

  private verifyValue(value, playerService: PlayerService) {
    let v = 0;
    if(isNumber(value * 1)) {
      v = value * 1;
    } else {
      v = this.getValue(value, playerService);
    }
    return v;
  }

  private increaseHP(value, playerService) {
    const v = this.verifyValue(value, playerService);
    playerService.hp += v;
    if(playerService.hp > playerService.maxHP) {
      playerService.hp = playerService.maxHP;
    }
  }

  private getValue(variable, playerService: PlayerService): number {
    const operand = variable.substring(1, variable.length - 1);
    return playerService[operand];
  }
}
