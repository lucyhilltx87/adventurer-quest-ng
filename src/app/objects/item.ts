
export class Item {
    name = '';
    key = '';
    description = '';
    chance = 0;
    value = 0;
    action = {};
    use = [];
    constructor(json) {
        for (let key in json) {
            if(this.hasOwnProperty(key)) {
                this[key] = json[key];
            }
        }
    }
}