
export class Quest {
    name = "";
    key = "";
    description = "";
    statisticImproved = "";
    statisticImprovedBy = 2;
    enemyBucket = '';
    numberOfEnemies = 0;
    itemBucket = '';
    numberOfEvents = 1;
    eventsBucket = '';
    level = 1;
    exp = 0;

    constructor(json) {
        console.log(json);
        for (let key in json) {
            if(this.hasOwnProperty(key)) {
                this[key] = json[key];
            }
        }
    }


}