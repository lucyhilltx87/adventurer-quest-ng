export class Bucket {
    name = '';
    contents = [];

    constructor(json) {
        for (let key in json) {
            if(this.hasOwnProperty(key)) {
                this[key] = json[key];
            }
        }
    }
}