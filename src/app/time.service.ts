import { Injectable } from '@angular/core';
import { Quest } from './objects/quest';
@Injectable({
  providedIn: 'root'
})
export class TimeService {
  eventName = '';
  eventFinish: number;
  eventProcessed = false;
  currentQuest: Quest = null;

  constructor() { }

  save() {
    localStorage.eventName = this.eventName;
    localStorage.eventFinish = this.eventFinish;
    localStorage.eventProcessed = this.eventProcessed;
    localStorage.currentQuest = JSON.stringify(this.currentQuest);
  }

  load() {
    this.eventName = localStorage.eventName;
    this.eventFinish = localStorage.eventFinish;
    this.eventProcessed = localStorage.eventProcessed === 'true';
    if(localStorage.currentQuest) {
      this.currentQuest = JSON.parse(localStorage.currentQuest);
    }
  }
}
