import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RefreshCharacterComponent } from './refresh-character.component';

describe('RefreshCharacterComponent', () => {
  let component: RefreshCharacterComponent;
  let fixture: ComponentFixture<RefreshCharacterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RefreshCharacterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RefreshCharacterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
