import { Component, OnInit } from '@angular/core';
import { PlayerService } from '../player.service';

@Component({
  selector: 'app-refresh-character',
  templateUrl: './refresh-character.component.html',
  styleUrls: ['./refresh-character.component.scss']
})
export class RefreshCharacterComponent implements OnInit {

  constructor(
    public playerService: PlayerService
  ) { }

  ngOnInit() {
    this.playerService.name = 'Adventurer';
    this.playerService.hp = 100;
    this.playerService.maxHP = 100;
    this.playerService.attack = 5;
    this.playerService.defense = 6;
    this.playerService.rest = 100;
    this.playerService.hunger = 100;
    this.playerService.exp = 0;
    this.playerService.silver = 0;
    this.playerService.items = [];
    this.playerService.availableQuests = [];
    this.playerService.finishedQuests = [];
    this.playerService.save();
  }

}
