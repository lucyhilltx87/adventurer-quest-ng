import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GameMenuComponent } from './game-menu/game-menu.component';
import { FinishTaskComponentComponent } from './finish-task-component/finish-task-component.component';
import { QuestChooserComponent } from './quest-chooser/quest-chooser.component';
import { QuestLogComponent } from './quest-log/quest-log.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RefreshCharacterComponent } from './refresh-character/refresh-character.component';
import { GenericModalComponent } from './generic-modal/generic-modal.component';
@NgModule({
  declarations: [
    AppComponent,
    GameMenuComponent,
    FinishTaskComponentComponent,
    QuestChooserComponent,
    QuestLogComponent,
    RefreshCharacterComponent,
    GenericModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [
    QuestChooserComponent,
    QuestLogComponent,
    GenericModalComponent
  ]
})
export class AppModule { }
